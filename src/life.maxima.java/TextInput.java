package life.maxima.java;

import java.util.Scanner;

public class TextInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        System.out.println("Enter your age");
//        int age = scanner.nextInt();
        int age = Integer.parseInt(scanner.nextLine());
//      "Masha\n20"
        System.out.println(name + ": " + age);

    }
}
