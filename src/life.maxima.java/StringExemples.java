package life.maxima.java;

import java.util.Arrays;

public class StringExemples {

    public static void main(String[] args) {
        String hello = "hello";
        System.out.println(hello.toUpperCase());
        System.out.println("Hello".isEmpty());
        System.out.println(hello.substring(1, 3));
        System.out.println("   ".isBlank());
        System.out.println("Hello".indexOf('l'));
        System.out.println("Hello".lastIndexOf('l'));
        System.out.println(Arrays.toString("Hello".toCharArray())); // строку превращает в массив символов
        System.out.println(Arrays.toString("Hello".split("l"))); // разделится на три части, если разделитель l

        System.out.println("".length());

        String str = "";
        for (int i = 1; i <= 10; i++) {
            str += i;
            str += ", ";
        }
        System.out.println(str.substring(0, 29)); // убрать последнюю запятую и пробел

        StringBuilder str2 = new StringBuilder();
        for (int i = 1; i <= 10; i++) {
            str2.append(i);
            str2.append(", ");
        }
        str2.delete(str2.length() -2, str2.length());
        System.out.println(str2);


        // null - переменная никуда не ссылается, объекта нет
//        String newString = null;
        String newString = "nulll";
        char[] charArray2 = null;

//        System.out.println(newString.toUpperCase()); // NPE

        System.out.println(newString != null ? newString.toUpperCase() : "manual NULL");
    }
}
