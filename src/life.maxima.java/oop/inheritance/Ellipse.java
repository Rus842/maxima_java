package life.maxima.java.oop.inheritance;

public class Ellipse extends Shape implements Scalable {


    private double r1;
    private double r2;

    public Ellipse() {

    }

    public double getR1() {
        return r1;
    }

    public Ellipse(double r1, double r2, int x, int y){
        setR1(r1);
        setR2(r2);
//        super(x, y);
//        this.r1 = r1;
//        this.r2 = r2;
        setX(x);
        setY(y);
    }


    public void setR1(double r1) {
        this.r1 = r1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    @Override
    public double area(){
        return Math.PI * r1 * r2;
    }

    @Override
    public void scale(int i) {

    }

    @Override
    public String toString() {
        return "Ellipse{"+"r1="+r1+", r2="+r2+'}';
    }

    @Override
    public void scale(double factor) {
        setR1(getR1()*factor);
        setR2(getR2()*factor);
    }
}
