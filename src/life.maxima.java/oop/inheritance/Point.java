package life.maxima.java.oop.inheritance;

public class Point extends Shape {

    public Point() {

    }

    public Point(int x, int y) {
        super(x, y);
    }

    @Override
    public double area() {

        return 0;
    }

    @Override
    public void scale(int i) {

    }

    @Override
    public String toString() {
        return super.toString();
    }


}
