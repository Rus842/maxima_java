package life.maxima.java;

public class LoopAndConditionals {
    public static void main(String[] args) {


        int a = 1;

//        if (true) {
//            System.out.println("!!!");
//        }
//        if (false)
//            System.out.println("???");
//
//            System.out.println("***");

        switch (a) {
            case 1:
                System.out.println("test 1");
                break;
            case 2:
                System.out.println("test2");
                break;
//            case 3,4,5:
//                System.out.println("test 3 4 5");
//                break;
            default:
                System.out.println("test default");
        }

        // в новой java
//        switch (a) {
//            case 1 -> System.out.println("test 1");
//                break;
//            case 2 -> System.out.println("test 2");
//            break;
////            case 3,4,5:
////                System.out.println("test 3 4 5");
////                break;
//            default -> System.out.println("test default");
//        }
//        System.out.println("______");
//        for (int i = 1; i <= 10; i++) {
//            System.out.println("Hello " + i);
//
//        }
//
//        int number = 1;
//
//        while (number <= 10) {
//            System.out.println(number++);
//        }
        System.out.println("______");

        long t1 = System.currentTimeMillis();

        int count = 0;
        while (System.currentTimeMillis() - t1 < 10) {
            System.out.println(++count);
//            if (count >= 200)
            if (true)
                break;
        }
        System.out.println("______");

        do {
            System.out.println(" do while");

        }while (false);







        }
    }

