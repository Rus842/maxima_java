package life.maxima.java.oop.inheritance;

public class Rectangle extends Shape implements Scalable{



    private double width;
    private double height;


    public Rectangle() {

    }

//    @Override
//    public double area() {
//        return 0;
//    }

    public Rectangle(double width, double height){
setWidth(width);
setHeight(height);
    }

    public Rectangle(double width, double height, int x, int y){
        setHeight(height);
        setWidth(width);
        setX(x);
        setY(y);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double area(){
        return width*height;
    }

    @Override
    public void scale(int i) {

    }

    @Override
    public void scale(double factor) {
//        height *=factor;
//        width *=factor;

        setWidth(getWidth()*factor);
        setHeight(getHeight()*factor);

    }
}
