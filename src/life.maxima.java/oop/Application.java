package life.maxima.java.oop;

import java.util.ArrayList;

public class Application {

    public static void main(String[] args) {
        new Human(); // создание объекта
        Human ivan = new Human(); // в переменную присваивается ссылка на объект
        ivan.setName("Ivan");
        ivan.setAge(30);

        System.out.println("Name " + ivan.getName());
        System.out.println("Age " + ivan.getAge());

        Human masha = new Human("Masha", 35);

        System.out.println("Name 2 " + masha.getName());
        System.out.println("Age 2 " + masha.getAge());



        // Создать класс Human, apllication. В методе main класса app..
        // создать список, с помощью цикла вводить с клавиатуры данные (имя, возраст)
        // завершить цикл командой Exit

        ArrayList<Human> list = new ArrayList<>();
        list.add(ivan);
        list.add(masha);

        for (Human h:list){
            System.out.println("");
        }
    }




}
