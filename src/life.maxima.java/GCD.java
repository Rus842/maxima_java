package life.maxima.java;

public class GCD {
    public static void main(String[] args) {

        int a = 106;
        int b = 16;

// ДЗ
// оптимальный вариант через вечный цикл
            while (true) {
                int c = a % b;
                if (c == 0) {
                    break;
                }

                a = b;
                b = c;
            }
        System.out.println(b);
    }

// 2.    если использовать while (r != 0), то int c = a % b выводится до while
// 3.    без if   while (b != 0) {
//                int c = a % b;
//                a = b;
//                b = c;
//}
//        System.out.println(a);
}

