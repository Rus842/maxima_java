package life.maxima.java.oop.inheritance;

public interface Scalable {

    void scale(double factor);
}
