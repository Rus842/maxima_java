package life.maxima.java.oop.inheritance;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        Rectangle r = new Rectangle();
        r.setWidth(10.5);
        r.setHeight(20.5);
        r.setX(10);
        r.setY(15);

        Ellipse e = new Ellipse();
        e.setR1(5);
        e.setR2(10);
        e.setX(10);
        e.setY(15);

        Object o = r;

        Shape s = Math.random() > 0.5 ? e : r;

        System.out.println(s.area());
        System.out.println(s);

        Circle c = new Circle();
        c.setR(10);

        System.out.println(c.area());

//        Square square = new Square();
//        square.setSide(10);
//        System.out.println(square.area());

        Circle circle = new Circle(5, 10, 10);
        System.out.println(circle);


        Rectangle r1 = new Rectangle(10, 15, 0, 0);
        Circle c1 = new Circle(20, 0, 0);

        System.out.println(r1);
        System.out.println(c1);

        r1.scale(2);
        c1.scale(2);

        Scalable scalable = r1;
        scalable.scale(2);

        scalable = c1;
        scalable.scale(2);

        System.out.println(r1);
        System.out.println(c1);

        System.out.println("_________");

        Circle newCircle = new Circle(10, 0, 0);
        Ellipse ellipse = newCircle;

        Rectangle rectangle = new Rectangle(20, 40, 0, 0);
        Object object = rectangle;

        Ellipse newEllipse = Math.random() > 0.5
                ? new Ellipse(20, 50, 0, 0)
                : new Circle(14, 0, 0);
        if (newEllipse instanceof Circle){
            Circle anothrCircle = (Circle)newEllipse;
            System.out.println("it's a circle");
            System.out.println(anothrCircle);
        } else {
            System.out.println("it's an ellipse");
            System.out.println(newEllipse);
        }
        System.out.println("-----");

        System.out.println(circle instanceof Circle);
        System.out.println(circle instanceof Ellipse);
        System.out.println(circle instanceof Shape);
        System.out.println(circle instanceof Object);
        System.out.println(circle instanceof Scalable);

        System.out.println(rectangle instanceof Square);
        System.out.println("hello" instanceof  Object);

        ArrayList<Shape> list = new ArrayList();
    }
}
