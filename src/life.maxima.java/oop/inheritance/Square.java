package life.maxima.java.oop.inheritance;

public class Square extends Rectangle{


    public double getSide(){
        return getHeight();
    }

    public void setSide(double side) {
        super.setHeight(side);
        super.setWidth(side);
    }

    @Override
    public void setHeight(double side) {
        setSide(side);
    }

    @Override
    public void setWidth(double side) {
        setSide(side);
    }

    @Override
    public String toString() {
        return "Square{"+"side=" + getHeight() + "}";

    }



}
