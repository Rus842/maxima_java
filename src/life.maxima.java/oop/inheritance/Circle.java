package life.maxima.java.oop.inheritance;

public class Circle extends Ellipse {



    public Circle(double r, int x, int y) {
        super(r, r, x, y);
    }

    public Circle() {
        super();
    }

    public double getR()
    { return getR2();}

    public void setR(double r){
        super.setR1(r);
        super.setR2(r);
    }


    @Override
    public void setR1(double r) {setR(r);}

    @Override
    public void setR2(double r) {setR(r);}

    @Override
    public String toString() {
        return "Circle{"+"r="+getR()+'}';
    }

    @Override
    public void scale(double factor) {
        setR(getR()*factor);
//        setR2(getR2()*factor);
    }
}
