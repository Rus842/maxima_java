package life.maxima.java.oop.inheritance;

import java.util.ArrayList;

public class HomeworkAbstraction {

    public static void main(String[] args) {
        ArrayList<Shape> list = new ArrayList();
        list.add(new Rectangle(10, 20, 0, 0));
        list.add(new Circle(15, 0, 0));
        list.add(new Point(0, 0));

        for (Shape s: list){
            if (s instanceof Scalable){
                ((Scalable)s).scale(2);
//                Scalable scalable = (Scalable)s;
//                scalable.scale(2);
            }
            System.out.println(s + "; area = " + s.area());
        }
        for (Shape s: list){
            System.out.println(s + "; area = " + s.area());
        }

//        for (Shape s: list) {
//            if (s instanceof Rectangle){
////                ((Rectangle)s).scale(2);
//                Rectangle r = (Rectangle)s;
//                r.scale(2);
//            } else if (s instanceof Circle){
//
//                ((Circle)s).scale(2);
//            }
//        }
    }
}
