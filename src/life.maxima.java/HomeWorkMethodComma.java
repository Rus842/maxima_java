package life.maxima.java;

import java.util.Scanner;

public class HomeWorkMethodComma {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String request = scanner.nextLine();
        System.out.println("Введите текст с запятыми:");
        request.toCharArray();
//        int counter = 0;

//        char[] chars = {'1', ',', ',', '.'};
//        String str = ",";

        System.out.println(countCommasAlt(request));
    }
//    public class CountCommas {
        public static int countCommas(String request) {
            int counter = 0;

            for (char c : request.toCharArray()) { // из строки получить массив
                if (c == ',') {
                    counter++;

                }

            }
            return counter;
        }

        public static int countCommasAlt(String request){
           return request.split(",").length - 1;

            }

    }