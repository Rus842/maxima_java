package life.maxima.java.oop;
import life.maxima.java.oop.Human;
import java.util.ArrayList;
import java.util.Scanner;

public class HomeworkOOP {

    // перенос литералов в константы
    private static final String PROMT = "\"Press 'enter' to continue or 'exit' to list all humans: \"";
    private static final String ENTER_THE_NAME = "Enter the name:";
    private static final String ENTER_THE_AGE = "Enter the age";

    public static void main(String[] args) {

        ArrayList<Human> list = new ArrayList<>();
//        list.add(ivan);
//        list.add(masha);
//
//        for (Human h:list){
//        System.out.println("");
//    }
        Scanner scanner = new Scanner(System.in);

        System.out.println(PROMT);

        while (!"exit".equalsIgnoreCase(scanner.nextLine())) {

            System.out.println(ENTER_THE_NAME);
            String name = scanner.nextLine();

//            if ("exit".equals(name));
//            if (name.equals("exit"));  //если будет null - ошибка


            System.out.println(ENTER_THE_AGE);
            int age = Integer.parseInt(scanner.nextLine());

            list.add(new Human(name, age));
            System.out.println(PROMT );

        }

        for (Human h: list){
            System.out.println(h.getName() + ": " + h.getAge());
        }
    }
}
