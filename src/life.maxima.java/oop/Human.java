package life.maxima.java.oop;

import java.util.Queue;

public class Human {

    String name;
    int age;

    public Human(){
        this("qqq", 36);
        System.out.println("Constructor");
    }

    public Human(String name, int age) {
//        this();
        setName(name);
        setAge(age);
    }

    //getter
   public String getName(){
       return name;
   }

    //setter
    public void setName(String name) {
       this.name = name;

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
       if (age <= 0){
           throw new IllegalArgumentException("Age must be positive");
       }
        this.age = age;
    }
}
